       IDENTIFICATION DIVISION.
       PROGRAM-ID.  WRITE-EMP1.
       AUTHOR. BENJAMAS. 

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT EMP-FILE ASSIGN TO "emp1.dat" 
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD EMP-FILE.
       01 EMP-DETAILS.
           88 END-OF-EMP-FILE VALUE HIGH-VALUE .
           05 EMP-SSN PIC 9(9).
           05 EMP-NAME.
              10 EMP-SIRNAME PIC X(15).
              10 EMP-FORENAME PIC X(10).
           05 EMP-DATE-OF-BIRTH.
              10 EMP-YOB PIC 9(4).
              10 EMP-MOB PIC 9(2).
              10 EMP-DOB PIC 9(2).
           05 EMP-GENDER PIC x.   

       PROCEDURE DIVISION .
       BEGIN.
           OPEN OUTPUT EMP-FILE
           MOVE "123456789" TO EMP-SSN
           MOVE "BENJAMAS" TO EMP-SIRNAME 
           MOVE "KRONJATUPHROM" TO EMP-FORENAME 
           MOVE "20010329" TO EMP-DATE-OF-BIRTH
           MOVE "F" TO EMP-GENDER
           WRITE EMP-DETAILS 

           MOVE "987654321" TO EMP-SSN
           MOVE "REN" TO EMP-SIRNAME 
           MOVE "ZOTTO" TO EMP-FORENAME  
           MOVE "20000327" TO EMP-DATE-OF-BIRTH
           MOVE "M" TO EMP-GENDER
           WRITE EMP-DETAILS

           MOVE "543216789" TO EMP-SSN
           MOVE "LUCA" TO EMP-SIRNAME 
           MOVE "KANISHIRO" TO EMP-FORENAME   
           MOVE "19990410" TO EMP-DATE-OF-BIRTH
           MOVE "M" TO EMP-GENDER
           WRITE EMP-DETAILS

           CLOSE EMP-FILE
           GOBACK
           .


