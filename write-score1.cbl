       IDENTIFICATION DIVISION. 
       PROGRAM-ID. write-score1.
       AUTHOR. Benjamas.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL.
           SELECT SCORE-FILE ASSIGN TO "score.dat" 
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD SCORE-FILE .
       01 SCORE-DETAIL.
           05 STU-ID PIC X(8).
           05 MIDTERM-SCORE PIC 9(2)V9(2).
           05 FINAL-SCORE PIC 9(2)V9(2).
           05 PROJECT-SCORE PIC 9(2)V9(2).

       PROCEDURE DIVISION .
       BEGIN.
           OPEN OUTPUT SCORE-FILE 

           MOVE "62160284" TO STU-ID  
           MOVE "34.05" TO MIDTERM-SCORE 
           MOVE "25.25" TO FINAL-SCORE 
           MOVE "10.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160285" TO STU-ID  
           MOVE "20" TO MIDTERM-SCORE 
           MOVE "10" TO FINAL-SCORE 
           MOVE "15" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160286" TO STU-ID  
           MOVE "30" TO MIDTERM-SCORE 
           MOVE "23" TO FINAL-SCORE 
           MOVE "11" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160287" TO STU-ID  
           MOVE "32" TO MIDTERM-SCORE 
           MOVE "21" TO FINAL-SCORE 
           MOVE "21" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160288" TO STU-ID  
           MOVE "30" TO MIDTERM-SCORE 
           MOVE "15" TO FINAL-SCORE 
           MOVE "25" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           CLOSE SCORE-FILE 
           GOBACK 
           .